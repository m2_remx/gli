package fr.istic.m2.controller.impl;

import fr.istic.m2.adapter.IAdapter;
import fr.istic.m2.controller.IController;
import fr.istic.m2.model.impl.Item;
import fr.istic.m2.view.IView;

import java.util.ArrayList;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines an Concrete Controller.
 */
public class ControllerImpl implements IController {

	/* Attributes */
	private IView view;
	private IAdapter adapter;

	/* Constructor */
	public ControllerImpl(IView v, IAdapter a) {
		this.adapter = a;
		this.view = v;
		this.view.setController(this);
		this.view.setAdapter(a);
		this.adapter.addObserver(this.view);
	}

	/* Getters & Setters */
	@Override
	public IAdapter getAdapter() {
		return this.adapter;
	}
	
	@Override
	public IView getView() {
		return this.view;
	}

	/* Controller Methods */
	@Override
	public void execute_AddNewItem(String name, String desc, int value) {
		boolean availableItemName = true;
		ArrayList<Item> items = new ArrayList<Item>(this.adapter.getItems());
		if(!items.isEmpty()) {
			for(Item item : items) {
				if (item.getName() == name) {
					availableItemName = false;
				}
			}
		}
		if(availableItemName) {
			this.adapter.addItem(new Item(name, desc, value));
		}
	}


}
