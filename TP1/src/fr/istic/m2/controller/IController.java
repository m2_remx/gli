package fr.istic.m2.controller;

import fr.istic.m2.adapter.IAdapter;
import fr.istic.m2.view.IView;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the Interface for the Controller of the application
 */
public interface IController {

	/* Getters */
	public IAdapter getAdapter();
	public IView getView();

	/* Controller Operations */
	public void execute_AddNewItem(String name, String desc, int value);


}
