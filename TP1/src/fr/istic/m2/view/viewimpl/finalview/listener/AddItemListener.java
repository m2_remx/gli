package fr.istic.m2.view.viewimpl.finalview.listener;

import fr.istic.m2.controller.IController;
import fr.istic.m2.view.viewimpl.finalview.FinalView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Listen addNewItem Events.
 */
public class AddItemListener implements ActionListener {

    // * Attributes * //
    private FinalView view;
    private IController controller;

    // * Constructor * //
    public AddItemListener(FinalView v, IController c) {
        this.view = v;
        this.controller = c;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = view.getItem_NameField();
        String desc = view.getItem_DescField();
        int value = view.getItem_ValueField();
        if(value == -1) {
            view.cancelFocus();
            /* Show an Error */
            return;
        }
        else if(name.isEmpty()) {
            view.cancelFocus();
            /* Show an Error */
            return;
        }
        else {
            controller.execute_AddNewItem(name, desc, value);
            view.cancelFocus();
        }

    }
}
