package fr.istic.m2.view.viewimpl.finalview;

import fr.istic.m2.adapter.IAdapter;
import fr.istic.m2.controller.IController;
import fr.istic.m2.view.IView;
import fr.istic.m2.view.components.ICamembert;
import fr.istic.m2.view.components.impl.CamembertFinalImpl;
import fr.istic.m2.view.viewimpl.finalview.listener.AddItemListener;
import fr.istic.m2.view.viewimpl.finalview.listener.CancelFocusListener;
import fr.istic.m2.view.viewimpl.finalview.listener.NavigationListener;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Final version of the View.
 */
public class FinalView extends JFrame implements IView {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // * MVC Attributes * //
    private IController controller;
    private IAdapter adapter;

    // * Attributes * //
    private JButton rightButton;
    private JButton leftButton;
    private ICamembert camembert;

    private JTextField item_NameField;
    private JTextField item_DescField;
    private JTextField item_ValueField;
    private JButton addItemButton;

    // * Constructor * //
    public FinalView(IAdapter a) {
        /* Init the window */
        super(" Camembert - Fernandez and Callimoutou ");
        this.setLayout(new BorderLayout());

        /* Init color generator */
        this.adapter = a;

        /* Init the navigation buttons */

        rightButton = new JButton(new ImageIcon("img/arrow-right.png")) ;
        rightButton.setText("right");
        rightButton.setHorizontalTextPosition(JButton.LEFT);
        rightButton.addActionListener(new NavigationListener(this));

        leftButton = new JButton(new ImageIcon("img/arrow-left.png")) ;
        leftButton.addActionListener(new NavigationListener(this));
        leftButton.setText("left");

        JPanel navigation_Buttons_Panel = new JPanel();
        navigation_Buttons_Panel.setLayout(new BorderLayout());
        navigation_Buttons_Panel.add(leftButton, BorderLayout.WEST);
        navigation_Buttons_Panel.add(rightButton, BorderLayout.EAST);
        /* -   -   -   -   */
        JPanel up_Panel = new JPanel();
        up_Panel.setLayout(new BorderLayout());
        up_Panel.setBackground(Color.LIGHT_GRAY);
        up_Panel.add(navigation_Buttons_Panel, BorderLayout.EAST );
        /* -   -   -   -   */


        /* Init the "AddNewItem" fields */
        item_DescField = new JTextField();
        item_NameField = new JTextField();
        item_ValueField = new JTextField();
        addItemButton = new JButton("Add Item");
        /* -   -   -   -   */
        JPanel down_Panel = new JPanel();
        down_Panel.setLayout(new GridLayout(1,4));
        down_Panel.add(item_NameField);
        down_Panel.add(item_DescField);
        down_Panel.add(item_ValueField);
        down_Panel.add(addItemButton);
        /* -   -   -   -   */

        /* Add the pie : Camembert */
        camembert = new CamembertFinalImpl(adapter.getTitle(), adapter.getTotalValues(), adapter.getItems(), adapter.getValues());
        this.add((JComponent) camembert, BorderLayout.CENTER);
        ((JComponent)camembert).setVisible(true);
        //((JComponent) camembert).addMouseListener(new CancelFocusListener(this));
        /* -   -   -   -   */
        this.add(up_Panel, BorderLayout.NORTH);
        this.add(down_Panel, BorderLayout.SOUTH);
        /* -   -   -   -   */
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setMinimumSize(new Dimension(300,300));
        this.setSize(600, 600);
    }




    // *  --  Getters  --  * //

    public String getItem_NameField() {
        return item_NameField.getText();
    }

    public String getItem_DescField() {
        return item_DescField.getText();
    }

    public int getItem_ValueField() {
        int value = -1;
        try{
            value = Integer.parseInt(item_ValueField.getText());
        }catch (NumberFormatException e){
            System.err.println(e.getMessage());
        }
        return value;
    }

    @Override
    public IController getController() {
        return this.controller;
    }

    @Override
    public IAdapter getAdapter() {
        return this.adapter;
    }


    // *  --  Setters  --  * //

    @Override
    public void setController(IController c) {
        this.controller = c;
        addItemButton.addActionListener(new AddItemListener(this, this.controller));
    }

    @Override
    public void setAdapter(IAdapter a) {
        this.adapter = a;
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void update(Observable o, Object arg) {
        /* Suppression du vieu fromage */
        this.remove(((BorderLayout)(this.getContentPane().getLayout())).getLayoutComponent(BorderLayout.CENTER));
        this.repaint();
        this.revalidate();

        /* Ajout du Camembert */
        CamembertFinalImpl camembert = new CamembertFinalImpl(adapter.getTitle(), adapter.getTotalValues(), adapter.getItems(), adapter.getValues());
        this.add( camembert, BorderLayout.CENTER);
        camembert.setVisible(true);

        this.repaint();
        this.revalidate();
    }




    /**
     * Set the focus on the next/previous Item.
     * @param right is a boolean that indicates to go right/left (next/previous) on the Camembert
     */
    public void changeFocusToRight(boolean right){
        camembert.changeFocusToRight(right);
        /*  -  Redraw everything  -  */
        this.repaint();
        this.revalidate();
    }

    /**
     * Cancel the focus.
     */
    public void cancelFocus(){
        this.camembert.cancelFocus();
        /*  -  Redraw everything  -  */
        this.repaint();
        this.revalidate();
    }

}
