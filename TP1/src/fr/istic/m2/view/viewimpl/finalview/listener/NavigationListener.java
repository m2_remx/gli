package fr.istic.m2.view.viewimpl.finalview.listener;

import fr.istic.m2.view.viewimpl.finalview.FinalView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Listen to Change Focus Event
 */
public class NavigationListener implements ActionListener {

    // * Attributes * //
    private FinalView view;

    // * Constructor * //
    public NavigationListener(FinalView v) {
        this.view = v;
    }



    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        view.changeFocusToRight(e.getActionCommand() == "right");
    }

}
