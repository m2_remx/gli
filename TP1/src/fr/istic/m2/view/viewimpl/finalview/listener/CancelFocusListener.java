package fr.istic.m2.view.viewimpl.finalview.listener;

import fr.istic.m2.adapter.IAdapter;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import fr.istic.m2.view.*;
import fr.istic.m2.view.viewimpl.finalview.FinalView;


/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the cancel focus listener
 * Whenever the user clicks wherever but on the pie, the focus has to be canceled
 */
public class CancelFocusListener implements MouseListener {

    // *  -  Attributes  -  * //
    private FinalView view ;

    // *  -  Constructor  -  * //
    public CancelFocusListener(FinalView v) {
        this.view = v;
    }

        @Override
    public void mouseClicked(MouseEvent e) {
        view.cancelFocus();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
