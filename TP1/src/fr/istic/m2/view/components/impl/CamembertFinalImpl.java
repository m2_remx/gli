package fr.istic.m2.view.components.impl;

import fr.istic.m2.model.impl.Item;
import fr.istic.m2.view.components.GenerateColor;
import fr.istic.m2.view.components.ICamembert;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.RoundRectangle2D;
import java.util.*;
import java.util.List;

import javax.swing.*;


/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the interface of a concrete CamembertFinalImpl component.
 */
public class CamembertFinalImpl extends JComponent implements ICamembert {

    /* ZOOM */
    private static final double ZOOM = (double)10/100;

    private class QuarterOfCamembert {
        /*  Attributes */
        private int index;
        private String name;
        private String description;
        private double value;
        private boolean focus;
        private int point;
        private double angle;
        private Color color;

        public Color getColor() {
            return color;
        }

        /*  Constructor */
        protected QuarterOfCamembert(int i, String n, String d, int v, Color c){
            this.index = i;
            this.name = n;
            this.description = d;
            this.value = v;
            this.focus = false;
            this.point = 0;
            this.angle = 0;
            this.color = c;
        }


        /* Getters */

        public int getIndex() {
            return index;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public double getValue() {
            return value;
        }

        public boolean isFocus() {
            return focus;
        }

        public double getAngle() {
            return angle;
        }

        /*  Setters  */

        public void setPoint(int point) {
            this.point = point;
        }

        public void setAngle(double angle) {
            this.angle = angle;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }
    }// - - - - -  End Of InnerClass QuarterPie  - - - - - //


    // *   - Attributes -   * //
    private String title;
    private int total;
    private GenerateColor colorgenerator;
    private static int focusindex;

    //private List<Item> items;
    private static List<QuarterOfCamembert> items;
    private static List<Double> values;
    private List<Color> colors;

    private HashMap<QuarterOfCamembert, Arc2D> mapItemsToQuarterGraphic;

    // *   - Constructor -   * //
    public CamembertFinalImpl(String title, int total, List<Item> items, List<Double> values){
        /*  New Features */
        this.addMouseListener(this);
        mapItemsToQuarterGraphic = new HashMap<QuarterOfCamembert,Arc2D>();
        /* - - - -  - - -*/
        this.title = title;
        this.items = new ArrayList<QuarterOfCamembert>();
        this.colors = new ArrayList<Color>();
        this.colorgenerator = new GenerateColor();
        for(int i=0; i<items.size(); i++) {
            Item item = items.get(i);
            this.items.add(new QuarterOfCamembert(i,item.getName(),item.getDescription(),item.getValue(), colorgenerator.next()));
        }
        //this.items = items;
        this.values = values;
        this.total = total;
        this.focusindex = -1;
    }

    @Override
    protected void paintComponent(Graphics g) {

        //*  --  Pie Parameters  --  *//
        int pie_width = (getSize().width)/2;
        int pie_height = (getSize().height)/2;

        //*  --  Center Circle  --  *//
        int centerCircle_width = pie_width/2;
        int centerCircle_height = pie_height/2;

        //*  --  Transparency Circle  --  *//

        int transparencyCircle_width = centerCircle_width+centerCircle_width/4;
        int transparencyCircle_height = centerCircle_height+centerCircle_height/4;
        // - * - - * - - * - - * - - //


        //*  --  Loop Drawing all Quarters of the Pie  --  *//
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int lastPoint = -270;

        for (int i = 0; i < values.size(); i++) {
            //  *  Fresh Add  *  //
            QuarterOfCamembert quarter = items.get(i);
            g2d.setColor(quarter.getColor());
            Double val = quarter.getValue();
            //  *     --      *  //

            //* Bring values to percentage *//
            val = val * 100 / total ;
            //*   -   -   -   -   -   -    *//

            Double angle = (val / 100) * 360;
            quarter.setAngle(angle);
            quarter.setPoint(lastPoint);

            if( focusindex == i ) {
                System.out.println(pie_width/2);
                Double zoom_x = (pie_width/2)-((pie_width/2)*ZOOM);

                Double zoom_y =  (pie_height/2)-(pie_height/2)*ZOOM;

                Double zoom_width = pie_width+(pie_width)*ZOOM;
                Double zoom_height =  pie_height+(pie_height)*ZOOM;

                //g2d.fillArc(zoom_x.intValue(), zoom_y.intValue(), zoom_width.intValue(), zoom_height.intValue(), lastPoint, -angle.intValue());
                Arc2D.Double slice = new Arc2D.Double(zoom_x.intValue(), zoom_y.intValue(), zoom_width.intValue(), zoom_height.intValue(), lastPoint, -angle.intValue(), Arc2D.PIE);
                mapItemsToQuarterGraphic.put(items.get(i), slice);
                g2d.fill(slice);

                showDetailOfItem(g,slice,items.get(focusindex));

            }
            else {
                //g2d.fillArc((pie_width/2), (pie_height/2), pie_width, pie_height, lastPoint, -angle.intValue());
                Arc2D.Double slice = new Arc2D.Double((pie_width/2), (pie_height/2), pie_width, pie_height, lastPoint, -angle.intValue(), Arc2D.PIE);
                mapItemsToQuarterGraphic.put(items.get(i), slice);
                g2d.fill(slice);
            }
            lastPoint = lastPoint + -angle.intValue();

        }

        //*  --  Name's Items  --  *//
        if( focusindex == -1 ) { //If any slice is select
            showNameOfItems(g);
        }

        //*  --  Transparency Circle  --  *//
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fillOval((11 * centerCircle_width/8) , (11 * centerCircle_height/8), transparencyCircle_width, transparencyCircle_height);

        //*  --  Center Circle  --  *//
        g2d.setColor(Color.BLACK);
        g2d.fillOval(centerCircle_width+(centerCircle_width/2), centerCircle_height+(centerCircle_height/2), centerCircle_width, centerCircle_height);

        //*  --  Title  --  *//
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Arial", Font.BOLD, 16));

        double title_height = g2d.getFontMetrics().getStringBounds(title, g2d).getHeight();
        double title_width = g2d.getFontMetrics().getStringBounds(title, g2d).getWidth();

        g2d.drawString(title, (int)(pie_width-title_width/2), (int)(pie_height-title_height/2) );

        //*  --  Total  --  *//
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Arial", Font.ITALIC, 13));

        String total_inEurosString = new String(" " + total + " euros");
        double total_height = g2d.getFontMetrics().getStringBounds(total_inEurosString, g2d).getHeight();
        double total_width = g2d.getFontMetrics().getStringBounds(total_inEurosString, g2d).getWidth();
        g2d.drawString(total_inEurosString, (int)(pie_width-total_width/2), (int)((pie_height-total_height/2) + title_height) );

    }

    /**
     * Show the Name of the different Item
     * @param g
     */
    private void showNameOfItems(Graphics g) {

        Iterator it = mapItemsToQuarterGraphic.entrySet().iterator();
        g.setFont(new Font("Arial", Font.BOLD, 15));
        FontMetrics fm = g.getFontMetrics();

        while( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) it.next();
            Arc2D slice = (Arc2D) entry.getValue();
            QuarterOfCamembert item = (QuarterOfCamembert)entry.getKey();
            String name = item.getName();


            int x_rect = (int) slice.getEndPoint().getX();
            int y_rect = (int) slice.getEndPoint().getY();

            //int x_rect = (int) ( (slice.getStartPoint().getX() + slice.getEndPoint().getX()) / 2) - 10;
            //int y_rect = (int) ( (slice.getStartPoint().getY() + slice.getEndPoint().getY()) / 2) - 10;

            int rect_width = fm.stringWidth(name) + 10;
            int rect_height = 20;


            //*  --  -- -- -- -- -- -- -- -- -- -- -- --  *//
           /* g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 15));

            double name_height = g.getFontMetrics().getStringBounds(name, g).getHeight();
            double name_width = g.getFontMetrics().getStringBounds(name, g).getWidth();
*/
            //g.drawString(title, (int)(pie_width-title_width/2), (int)(pie_height-title_height/2) );
            //*  --  -- -- -- -- -- -- -- -- -- -- -- --  *//

            Rectangle area = getBounds();


            if(x_rect < (area.width/2) ) {
                x_rect = x_rect - rect_width -10;
            }

            if(y_rect < area.height /2){
                y_rect = y_rect - rect_height;
            }

            int x_text = fm.stringWidth(name);

            //int y_text =  y_rect+5;

            g.setColor(item.getColor());
            g.fillRect(x_rect, y_rect,x_text+5, rect_height);
            g.drawLine((int)slice.getEndPoint().getX(), (int)slice.getEndPoint().getY(), x_rect, y_rect);
            //((Graphics2D)g).fill(new RoundRectangle2D.Double(x_rect, y_rect, x_text+5,rect_height, 10, 10));
            g.setColor(Color.WHITE);

            g.drawString(name, x_rect+2, y_rect+15);

        }
    }

    private void showDetailOfItem(Graphics g, Arc2D slice, QuarterOfCamembert item) {

        Iterator it = mapItemsToQuarterGraphic.entrySet().iterator();
        g.setFont(new Font("Arial", Font.BOLD, 15));
        FontMetrics fm = g.getFontMetrics();

        //while( it.hasNext() ) {
            //Map.Entry entry = (Map.Entry) it.next();
            //slice = (Arc2D) entry.getValue();

            String name = item.getName();

            int x_rect = (int) slice.getEndPoint().getX();
            //int y_rect = (int) slice.getEndPoint().getY();

            int y_rect = (int) ( (slice.getStartPoint().getY() + slice.getEndPoint().getY()) /2)-15;

            int rect_width = fm.stringWidth(name) + 10;
            int rect_height = 60;


            Rectangle area = getBounds();

            if(x_rect < area.width/2)
                x_rect  -= rect_width;

            if(y_rect < area.height /2)
                y_rect -= rect_height;

            int x_text = fm.stringWidth(item.getDescription());

            //int y_text =  y_rect+5;

            g.setColor(item.getColor());
            g.fillRect(x_rect, y_rect,x_text+5, rect_height);

            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 15));
            g.drawString(name, x_rect+2, y_rect+15);

            g.drawString(new Double(item.getValue()).toString(), x_rect+2, y_rect+30);
            g.drawString(item.getDescription(), x_rect+2, y_rect+45);

        }
   //}

    @Override
    public void setItems(List<Item> items) {
        //this.items = items;
    }

    @Override
    public void addItem(Item item) {
        //this.items.add(item);
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getFocus() {
        return 0;
    }

    @Override
    public void setFocusOnItem(int index) {
        this.items.get(index).setFocus(true);
    }

    @Override
    public void changeFocusToRight(boolean right) {
        if(focusindex < 0) {
            focusindex = 0;
        }
        else if(right){
            focusindex = (focusindex+1) % items.size();
        }
        else {
            focusindex = ((((focusindex-1)% items.size()) + items.size()) % items.size()) ;
        }
        System.out.println("    -------- >" + focusindex);
        this.repaint();
        this.revalidate();
    }

    @Override
    public void cancelFocus() {
        this.focusindex = -1;

        this.repaint();
        this.revalidate();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        boolean cancel_focus = true;
        Iterator it = mapItemsToQuarterGraphic.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry entry = (Map.Entry)it.next();
            if (((Arc2D)entry.getValue()).contains(e.getX(), e.getY()) ) {
                focusindex = ((QuarterOfCamembert)entry.getKey()).getIndex();
                this.repaint();
                this.revalidate();
                cancel_focus = false;
            }
        }
        if(cancel_focus){
            cancelFocus();
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
