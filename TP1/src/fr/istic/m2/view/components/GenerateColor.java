package fr.istic.m2.view.components;

import java.awt.*;

/**
 * Created by callimom on 26/10/15.
 */
public class GenerateColor {

    //**  --  Constructor  --  **//
    static private final int NUMBER_OF_COLORS = 10;
    private int index;

    //* Table of colors *//
    private Color[] colors;

    //**  --  Constructor  --  **//
    public GenerateColor() {
        index = 0;
        colors = new Color[NUMBER_OF_COLORS];

        colors[0] = new Color(95,158,160);
        colors[1] = new Color(152,245,255);
        colors[2] = new Color(66,66,111);
        colors[3] = new Color(100,149,237);
        colors[4] = new Color(173,216,230);
        colors[5] = new Color(209,238,238);
        colors[6] = new Color(0,0,205);
        colors[7] = new Color(65,86,197);
        colors[8] = new Color(89,89,171);
        colors[9] = new Color(112,219,147);
    }

    public Color next(){
        index = (index+1) % NUMBER_OF_COLORS;
        return colors[index];

    }
}
