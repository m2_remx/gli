package fr.istic.m2.view.components;

import fr.istic.m2.model.impl.Item;

import java.awt.event.MouseListener;
import java.util.List;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the interface of Camembert components.
 */
public interface ICamembert extends MouseListener {

    public void setItems(List<Item> items);

    public void addItem(Item item);

    public void setTitle(String title);

    public int getFocus();

    public void setFocusOnItem(int index);

    public void changeFocusToRight(boolean right);

    public void cancelFocus();

}