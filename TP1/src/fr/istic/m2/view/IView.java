package fr.istic.m2.view;

import fr.istic.m2.adapter.IAdapter;
import fr.istic.m2.controller.IController;

import java.util.Observer;

/**
 * Created by Monica and Mickael.
 * Defines the interface for the Views of the application.
 */
public interface IView extends Observer {

    /* Getters & setters */
    public IController getController();
    public void setController(IController c);

    public IAdapter getAdapter();
    public void setAdapter(IAdapter a);

    public void showError(String message);

}
