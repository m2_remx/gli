package fr.istic.m2.main;

import fr.istic.m2.adapter.*;
import fr.istic.m2.adapter.impl.AdapterImpl;
import fr.istic.m2.controller.*;
import fr.istic.m2.controller.impl.ControllerImpl;
import fr.istic.m2.model.impl.ModelWithItems;
import fr.istic.m2.view.*;
import fr.istic.m2.view.viewimpl.finalview.FinalView;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Main Class - Main Method
 */
public class Main {
	
	public static void main(String args[]) {

		IAdapter a = new AdapterImpl(new ModelWithItems());
		IView v = new FinalView(a);
		IController controller = new ControllerImpl(v, a);
		
	}
	
}