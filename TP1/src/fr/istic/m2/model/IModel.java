package fr.istic.m2.model;

import fr.istic.m2.model.impl.Item;

import java.util.List;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the Interface for the Model of our application.
 */
public interface IModel {

    /* -- Getters & Setters */
    public String getTitle();

    public List<Item> getItems();
    public void setItems(List<Item> list);
    public void addItem(Item item);

    public Item getItem(int index);
    public Item getItem(String name);

    public List<Double> getValues();
    public List<String> getItemsNames();
    public List<String> getItemsDescs();

    public int getTotalValues();

    /* -- Remove Items -- */
    public boolean deleteItem_withIndex(int index);
    public boolean deleteItem_withName(String name);
    public boolean deleteItem(Item item);

    /* -- Update Items -- */
    public boolean replaceItem(int index, Item item);
    public boolean updateItemDescription_withIndex(int index, String description);
    public boolean updateItemDescription_withName(String name, String description);
    public boolean updateItemName(int index, String name);

}
