package fr.istic.m2.model.impl;


/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines an Item.
 */
public class Item {
	
	// * Attributes * //
	private String name;
	private String description;
	private int value;
	
	// * Constructors * //
	public Item(String n, String desc, int v) {
		this.name = n;
		this.description = desc;
		this.value = v;
	}

	// * Getters & Setters * //
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
}
