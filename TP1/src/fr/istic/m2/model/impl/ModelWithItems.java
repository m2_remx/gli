package fr.istic.m2.model.impl;

import fr.istic.m2.model.IModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines a concrete Model with some example items in it.
 */
public class ModelWithItems implements IModel{

    /* Attributes */
    private List<Item> items;
    private String title;

    /* Constructor */
    public ModelWithItems() {
        this.title = "Pokemon";
        items = generateItems();
    }

    /**
     * @return a model with five items in it.
     */
    private List<Item> generateItems() {
        ArrayList<Item> items = new ArrayList<Item>();
        Item i1 = new Item("Psykokwak","Best Pokemon Ever", 40);
        Item i2 = new Item("Ronflex","Best Sleeper Ever", 20);
        Item i3 = new Item("Carapuce","Water Starter", 25);
        Item i4 = new Item("Salameche","Fire Starter", 15);
        Item i5 = new Item("Bulbizarre","Plant Starter", 5);
        items.add(i1);
        items.add(i2);
        items.add(i3);
        items.add(i4);
        items.add(i5);

        return items;
    }

    /*  -  Override Methods  -  */

    /* Getters & Setters */
    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public List<Item> getItems() {
        return items;
    }

    @Override
    public void setItems(List<Item> list) {
        items = list;
    }

    @Override
    public Item getItem(int index) {
        return items.get(index);
    }

    @Override
    public Item getItem(String name) {
        return null;
    }

    @Override
    public List<Double> getValues() {
        List<Double> values = new ArrayList<Double>();
        for(int i=0; i< items.size(); i++) {
            values.add((double)items.get(i).getValue());
        }
        return values;
    }

    @Override
    public List<String> getItemsNames() {
        List<String> names = new ArrayList<String>();
        for(int i=0; i< items.size(); i++) {
            names.add(items.get(i).getName());
        }
        return names;
    }

    @Override
    public List<String> getItemsDescs() {
        List<String> descs = new ArrayList<String>();
        for(int i=0; i< items.size(); i++) {
            descs.add(items.get(i).getDescription());
        }
        return descs;
    }

    @Override
    public int getTotalValues() {
        int total = 0;
        for(int i=0; i< items.size(); i++) {
            total += items.get(i).getValue();
        }
        return total;
    }

    @Override
    public boolean deleteItem_withIndex(int index) {
        return (items.remove(index) == null ) ? false : true ;
    }

    @Override
    public boolean deleteItem_withName(String name) {
        return false;
    }

    @Override
    public void addItem(Item item) {
        items.add(item);
    }

    /* -- Remove Items -- */

    @Override
    public boolean deleteItem(Item item) {
        return items.remove(item) ;
    }

    /* -- Update Items -- */
    @Override
    public boolean replaceItem(int index, Item item) {
        if (items.get(index) != null) {
            items.set(index, item);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateItemDescription_withIndex(int index, String description) {
        Item item = items.get(index);
        if ( item != null) {
            item.setDescription(description);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateItemDescription_withName(String name, String description) {
        return false;
    }


    @Override
    public boolean updateItemName(int index, String name) {
        Item item = items.get(index);
        if ( item != null) {
            item.setName(name);
            return true;
        }
        return false;
    }


}

