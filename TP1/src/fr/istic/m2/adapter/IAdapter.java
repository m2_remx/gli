package fr.istic.m2.adapter;

import fr.istic.m2.model.IModel;
import fr.istic.m2.model.impl.Item;

import java.util.List;
import java.util.Observer;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines the Interface of a model AdapterImpl.
 * The AdapterImpl is the component that will link the Model to the Controller.
 *  /!\ Concrete AdapterImpl also has to extends Observable /!\
 */
public interface IAdapter extends IModel {

    /* This Method is also here to remind that the concrete AdapterImpl has to extends the Observable class */
    void addObserver(Observer observer);
}
