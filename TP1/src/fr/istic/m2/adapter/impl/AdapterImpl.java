package fr.istic.m2.adapter.impl;

import java.util.List;
import java.util.Observable;

import fr.istic.m2.adapter.IAdapter;
import fr.istic.m2.model.IModel;
import fr.istic.m2.model.impl.Item;

/**
 * Created by Monica and Mickael on 14/10/15.
 * Defines an Concrete AdapterImpl.
 */
public class AdapterImpl extends Observable implements IAdapter {

	/* Adaptee */
	private IModel model;

	// * Constructors * //
	public AdapterImpl(IModel m) {
		this.model = m;
	}


	@Override
	public String getTitle() {
		return model.getTitle();
	}

	// * Getters & Setters * //
	@Override
	public List<Item> getItems() {
		return model.getItems();
	}

	@Override
	public void setItems(List<Item> list) {
		model.setItems(list);
		notifyController();
	}

	@Override
	public Item getItem(int index) {
		return model.getItem(index);
	}

	@Override
	public Item getItem(String name) {
		return model.getItem(name);
	}

	@Override
	public List<Double> getValues() {
		return model.getValues();
	}

	@Override
	public List<String> getItemsNames() {
		return model.getItemsNames();
	}

	@Override
	public List<String> getItemsDescs() {
		return model.getItemsDescs();
	}

	@Override
	public int getTotalValues() {
		return model.getTotalValues();
	}

	@Override
	public boolean deleteItem_withIndex(int index) {
		if (model.deleteItem_withIndex(index)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean deleteItem_withName(String name) {
		if (model.deleteItem_withName(name)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public void addItem(Item item) {
		model.addItem(item);
		notifyController();
	}


	@Override
	public boolean deleteItem(Item item) {
		if (model.deleteItem(item)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean replaceItem(int index, Item item) {
		if (model.replaceItem(index, item)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean updateItemDescription_withIndex(int index, String description) {
		if (model.updateItemDescription_withIndex(index, description)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean updateItemDescription_withName(String name, String description) {
		if (model.updateItemDescription_withName(name, description)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean updateItemName(int index, String name) {
		if (model.updateItemName(index, name)) {
			notifyController();
			return true ;
		}
		else {
			return false;
		}
	}


    /**
	 * Notify the Observers (the Views), that a changes has occured
	 * Basically, this method is called only when a Write/Set/Update method was called
	 */
	private void notifyController() {
		setChanged();
		notifyObservers();
	}

}
