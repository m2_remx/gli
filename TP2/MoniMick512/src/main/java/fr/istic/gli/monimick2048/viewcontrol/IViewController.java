package fr.istic.gli.monimick2048.viewcontrol;

import fr.istic.gli.monimick2048.controller.IController;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */
public interface IViewController extends Initializable {

    /**
     * Initialize the Model (IBoard) and Refresh the View (viewBoard).
     * @param location
     * @param resources
     */
    @Override
    void initialize(URL location, ResourceBundle resources);


    /**
     * This Method is called by the IController and Refresh this View
     * @param tiles : list of all non-empty Tiles in the current Board
     *              [0 -> line ; 1 -> column ; 2-> rank]
     */
    void updateView(ArrayList<int[]> tiles);


    /**
     * Set a real controller to this ViewCtrl
     * Also init
     * @param controller
     */
    void setController(IController controller);

    /**
     * KeyPressed Event Listener
     * @param event
     */
    void keyPressed(KeyEvent event);

    /**
     * TODO: "NewGame Listener Button"
     * If a NewGame button is added to the GUI
     * @param event
     */
    void newGameButtonAction(ActionEvent event);

    /**
     * TODO: "Action Event Listener Button"
     * If you want CONTROLS in your GUI
     * @param event
     */
    void handleDirectionButtonAction(ActionEvent event);


    /**
     * The Game Is OVER...
     */
    void gameIsOver();
}
