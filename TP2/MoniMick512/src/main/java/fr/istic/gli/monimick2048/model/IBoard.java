package fr.istic.gli.monimick2048.model;

import fr.istic.gli.monimick2048.controller.IController;

/**
 * Created by plouzeau on 2014-10-09.
 * A square board from hosting tiles.
 */
public interface IBoard {


    /**
     * numbers of tiles, squares in one line (or column) of our square Board
     * @return numbers of tiles in one line of our Board.
     */
    int getSideSizeInSquares();

    /**
     * Return the tile at a given coordinate, or null if none exists there.
     *
     * @param lineNumber   must be >=1 and <= getSideSizeInSquares()
     * @param columnNumber must be >=1 and <= getSideSizeInSquares()
     * @return a tile or null if none
     * @throws java.lang.IllegalArgumentException if parameters are out of board's bounds
     */
    ITile getTile(int lineNumber, int columnNumber);

    /**
     * Direction in which is possible to play
     */
    enum Direction {
        LEFT, RIGHT, TOP, BOTTOM;
    }

    /**
     * Basic Action of the game.
     * Apply the only game action: packing tiles.
     * @param direction  where to push the tiles
     */
    void packIntoDirection(Direction direction);


    //* ____________________________________________ *//
    //* - -                                      - - *//
    //* - - -  Added By Monica F. & Mickael C. - - - *//
    //* -                                          - *//
    //* ____________________________________________ *//

    /**
     * Initialization of the IBoard
     * @return Table[][] containing coordinates of the default tiles
     */
    void defaultInit();

    /**
     * Return the current state of the Model (the IBoard)
     * @return the IBoard : Table[][] of TILEs
     */
    ITile[][] getBoard();


    /**
     * @return True if the Game is Over
     */
    boolean gameIsOver();


    /**
     * Set the Controller to the Model
     * @param controller
     */
    void setController(IController controller);


    /**
     *
     * @param line
     * @param column
     * @return
     */
    boolean tileIsNull(int line, int column);


    /**
     *
     * @param line
     * @param column
     * @return
     */
    int getRankOfTile(int line, int column);
}

