package fr.istic.gli.monimick2048;

import fr.istic.gli.monimick2048.controller.IController;
import fr.istic.gli.monimick2048.controller.impl.ControllerImpl;
import fr.istic.gli.monimick2048.viewcontrol.IViewController;
import fr.istic.gli.monimick2048.model.IBoard;
import fr.istic.gli.monimick2048.model.impl.BoardImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

/**
 * Min 2048
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see javafx.application.Application
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //*   -   -   -   -   -   -   -   *//
        URL location = getClass().getResource("/sample.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        Parent root = fxmlLoader.load();

        //* - - -  Model View Controller  - - - *//
        IViewController viewCtrl = fxmlLoader.getController();
        IBoard board = new BoardImpl(4);
        IController controller = new ControllerImpl();

        controller.setBoard(board);
        controller.setView(viewCtrl);
        //*   -   -   -             -   -   -   *//

        primaryStage.setTitle("2048 ReMx - Callimoutou Fernandez");
        primaryStage.setResizable(false);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        scene.getRoot().requestFocus();

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
