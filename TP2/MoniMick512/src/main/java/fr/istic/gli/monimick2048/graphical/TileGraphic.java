package fr.istic.gli.monimick2048.graphical;

import javafx.scene.control.Label;


/**
 * This class is just an attempt to personalize the application
 * We used Pokemon images...
 * /!\ We don't own any rights on pictures in resources/pokemon/*  /!\
 * Please, if you don't want to see any Pokemon, change the Style to DEFAULT.
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see javafx.scene.control.Label
 */


public class TileGraphic extends Label {

    //* Final Size of the ITile Graphical Component (Label) *//
    private static final int TILE_HEIGHT_SIZE_IN_PX = 140;
    private static final int TILE_WIDTH_SIZE_IN_PX = 140;

    //* Rank of the ITile *//
    int rank;


    //*   - Constructor -   *//
    public TileGraphic(int r) {
        super();
        rank = r;

        /* /!\ If you are tired of seeing Pokemons, we create a Default Version of the Game /!\ */
        /* You just have to uncomment the next instructions and comment the newGeneratePokemon instruction */
        //String style = generateStyle(new GenerateDefault());

        //String style = defaultStyle("default", r);
        String style = defaultStyle("pokemon", r);

        this.setStyle(style);
    }


    /**
     *
     *  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
     * |   It is important to note that the resource folder has to have a strict structure  *
     * |   under the folder of the "theme" or "style", it should have ONE FOLDER PER RANK   *
     * |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
     *
     * @param folderName Folder with the images
     * @param rank
     * @return
     */
    private String defaultStyle(String folderName, int rank){
        return "-fx-background-image: url(" + generateURL(folderName, rank) + ") ;" +
                "    -fx-min-height: "+ TILE_HEIGHT_SIZE_IN_PX +"px;" +
                "    -fx-min-width: "+ TILE_WIDTH_SIZE_IN_PX +"px;" +
                "    -fx-border-color: #000000;";
    }


    /**
     * Generate image's URL
     * @param folderName Folder with the images
     * @param rank
     * @return a random URL of an image file contain in
     */
    private String generateURL(String folderName, int rank) {
       /* File folder;
        String url = null;
        try {
            folder = new File(getClass().getResource("/"+folderName+"/"+rank+"/").toURI());
            File[] files = folder.listFiles();
            int index = new Random().nextInt(files.length);
            url = "/"+ folderName +"/"+rank+"/"+files[index].getName();
        } catch (URISyntaxException e) {
            System.out.println(" Crash Generating "+ folderName +" Image ["+e.getMessage()+"]");
        }

        return url ;*/
        return "/"+folderName+"/"+rank+"/"+rank+".png";
    }
}
