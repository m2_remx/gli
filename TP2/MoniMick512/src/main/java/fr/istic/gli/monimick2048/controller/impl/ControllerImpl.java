package fr.istic.gli.monimick2048.controller.impl;

import fr.istic.gli.monimick2048.controller.IController;
import fr.istic.gli.monimick2048.model.IBoard;
import fr.istic.gli.monimick2048.model.ITile;
import fr.istic.gli.monimick2048.viewcontrol.IViewController;

import java.util.ArrayList;

/**
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see fr.istic.gli.monimick2048.controller.IController
 */
public class ControllerImpl implements IController {

    //*     -   Attributes   -     *//
    private IViewController view;
    private IBoard model;


    //*     -   Setters   -     *//
    @Override
    public void setView(IViewController vc) {
        this.view = vc;
        this.view.setController(this);
    }

    @Override
    public void setBoard(IBoard m) {
        this.model = m;
        this.model.setController(this);
    }

    @Override
    public void updateView(ITile[][] board) {

        //* Give the view only the non-null Tiles *//
        /**
         * int[0->Line; 1->Column; 2-> Rank]
         */
        ArrayList<int[]> tiles = new ArrayList<>();

        for(int line=1 ;line <= board.length ; line++) {
            for(int column=1 ; column <= board.length ; column++) {
                if( ! model.tileIsNull(line, column)) {
                    tiles.add(new int[]{line-1, column-1, model.getRankOfTile(line, column)});
                }
            }
        }

        this.view.updateView(tiles);
    }


    @Override
    public void gameIsOver() {
        this.view.gameIsOver();
    }

    @Override
    public void initModel() {
        model.defaultInit();
    }

    @Override
    public void moveTilesTo(Direction direction) {
        IBoard.Direction d = null;
        switch (direction) {
            case UP :
                d = IBoard.Direction.TOP;
                break;
            case DOWN:
                d = IBoard.Direction.BOTTOM;
                break;
            case LEFT:
                d = IBoard.Direction.LEFT;
                break;
            case RIGHT:
                d = IBoard.Direction.RIGHT;
                break;
        }
        if (d != null) {
            model.packIntoDirection(d);
        }

    }
}
