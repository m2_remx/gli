package fr.istic.gli.monimick2048.model;

/**
 * Created by plouzeau on 2014-10-09.
 */
public interface ITile {

     /** 2 a la puissance Rank
     *
     */
    int getRank();

    void incrementRank();
}
