package fr.istic.gli.monimick2048.controller;

import fr.istic.gli.monimick2048.model.IBoard;
import fr.istic.gli.monimick2048.model.ITile;
import fr.istic.gli.monimick2048.viewcontrol.IViewController;

/**
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 */

public interface IController {

    /* Setters */
    void setView(IViewController vc);
    void setBoard(IBoard m);

    /**
     * Reads the Model (The Board) and Refresh the View
     * @param board the Model
     */
    void updateView(ITile[][] board);


    /**
     *  Ends the application
     */
    void gameIsOver();

    /**
     * Initialize the Board and return the coordinate(and rank) of the two default tiles.
     */
    void initModel();

    /**
     *
     * @param direction to pack the Tiles into
     */
    void moveTilesTo(Direction direction);

    /**
     * Enum Direction used in the event listener in the View
     * Direction in which is possible to play
     */
    enum Direction {
        LEFT, RIGHT, UP, DOWN, NONE
    }
}
