package fr.istic.gli.monimick2048.model.impl;

import fr.istic.gli.monimick2048.model.ITile;

/**
 * Created by plouzeau on 2014-10-09.
 */
public class TileImpl implements ITile {

    private  int rank;

    public TileImpl(int rank) {
        this.rank = rank;
    }

	public int getRank() {
		return rank;
	}

	public void incrementRank() {
		rank++;
	}

	public boolean equals(ITile obj) {
		return this.rank == obj.getRank();
	}
}
