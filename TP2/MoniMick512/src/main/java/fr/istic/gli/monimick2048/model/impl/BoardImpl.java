package fr.istic.gli.monimick2048.model.impl;

import fr.istic.gli.monimick2048.controller.IController;
import fr.istic.gli.monimick2048.model.IBoard;
import fr.istic.gli.monimick2048.model.ITile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Created by plouzeau on 2014-10-09.
 * Modified by Mónica Fernández & Mickaël CALLIMOUTOU
 */
public class BoardImpl implements IBoard {

    /*   - -   Attributes  - -  */
    private IController controller;

    private final int sideSizeInSquares;
    private Direction directionToPackInto;
    private boolean theBoardHasChanged;

    /* -- Constructor -- */
    public BoardImpl(int sideSizeInSquares) {
        if (sideSizeInSquares <= 1) {
            throw new IllegalArgumentException("sideSizeInSquares");
        }
        this.sideSizeInSquares = sideSizeInSquares;
        currentBoard = new ITile[sideSizeInSquares][sideSizeInSquares];
        nextBoard = new ITile[sideSizeInSquares][sideSizeInSquares];
        theBoardHasChanged = false;
    }


    /* -- Getter -- */
    public int getSideSizeInSquares() {
        return this.sideSizeInSquares;
    }

    /* -- Two Boards -- */
    private ITile[][] currentBoard;
    private ITile[][] nextBoard;


    /**
     * Return the tile at a given coordinate, or null if none exists there.
     *
     * @param lineNumber   must be >=1 and <= getSideSizeInSquares()
     * @param columnNumber must be >=1 and <= getSideSizeInSquares()
     * @return a tile or null if none
     * @throws IllegalArgumentException if parameters are out of board's bounds
     */
    public ITile getTile(int lineNumber, int columnNumber) {
        return currentBoard[lineNumber - 1][columnNumber - 1];
    }

    /**
     * Apply the only game action: packing tiles
     * @param direction  where to push the tiles
     */
    public void packIntoDirection(Direction direction) {
        this.directionToPackInto = direction;
        theBoardHasChanged = false;
        for (int i = 1; i <= sideSizeInSquares; i++) {
            packLine(i);
        }
        if( theBoardHasChanged ) {
            popANewTileInBoard(nextBoard);
        }
        else {
            /* if no Tile has appeared, it possibly means that the Game is over */
            if ( gameIsOver() ) {
                /**/
                for (int i = 0; i < sideSizeInSquares; i++) {
                    StringBuffer outputBuffer = new StringBuffer();
                    outputBuffer.append(i + 1);
                    outputBuffer.append(":{");
                    for (int j = 0; j < sideSizeInSquares; j++) {
                        if (currentBoard[i][j] != null) {
                            outputBuffer.append(currentBoard[i][j].getRank());
                        } else {
                            outputBuffer.append("0");
                        }
                    }
                    outputBuffer.append("}");
                }
                controller.gameIsOver();
            }
        }
        commit();
    }


    /**
     * Validate the step effects
     * Validate the new Board and erase the old one
     * Also tells the Controller to refresh the View
     */
    public void commit() {
        currentBoard = nextBoard;
        nextBoard = new ITile[sideSizeInSquares][sideSizeInSquares];

        //* Only when a real move was made, The controller has to update the View *//
        if (theBoardHasChanged) {
            controller.updateView(currentBoard);
        }
    }


    private void packLine(int lineNumber) {
      /*
      * Scan the current board line looking for two consecutive tiles
      * with the same rank
      * When this case is encountered, write a single tile with rank+1
      * Otherwise just copy the tile (in practice packing it in the next board)
      * Remember that indices are 1-based in this code
      * Conversion to Java arrays indices is done in computeLineIndex and computeColumnIndex
      */
        int readIndex = 1; // Position of the tile to be read
        int writeIndex = 0; // Position of the last tile written

        while (readIndex <= sideSizeInSquares) {
            // Find next tile
            while ((readIndex <= sideSizeInSquares)
                    && (readTile(currentBoard, lineNumber, readIndex) == null)) {
                readIndex++;
            }
            if (readIndex > sideSizeInSquares) {
                break; // Done with the line
            }
            // Try to merge with previous tile
            if ((writeIndex > 0) &&
                    (readTile(nextBoard, lineNumber, writeIndex).getRank()
                            == readTile(currentBoard, lineNumber, readIndex).getRank())) {
                // Merge previously written tile and currently read one
                readTile(nextBoard, lineNumber, writeIndex).incrementRank();
                theBoardHasChanged = true;
            } else {
                // Advance write index and copy currently read tile
                writeIndex++;
                writeTile(nextBoard, readTile(currentBoard, lineNumber, readIndex), lineNumber, writeIndex);
                if(readIndex != writeIndex) {
                    theBoardHasChanged = true;
                }
            }
            // Done with the current tile read, move forward
            readIndex++;
        }
    }

    /**
     * Writes a tile into a matrix (board) using indices transformation
     * @param board       destination
     * @param tile        what to write at the given coordinates
     * @param lineIndex   coordinate
     * @param columnIndex  coordinate
     */
    private void writeTile(ITile[][] board, ITile tile, int lineIndex, int columnIndex) {
        board[computeLineIndex(lineIndex, columnIndex)][computeColumnIndex(lineIndex, columnIndex)] = tile;
    }

    /**
     * Returns a tile  from a matrix (board) using indices transformation
     * @param board      origin
     * @param lineIndex   coordinate
     * @param columnIndex  coordinate
     * @return    tile at the given coordinates or null if no tile there
     */
    private ITile readTile(ITile[][] board, int lineIndex, int columnIndex) {
        int boardLineIndex = computeLineIndex(lineIndex, columnIndex);
        int boardColumnIndex = computeColumnIndex(lineIndex, columnIndex);
        ITile currentTile = board[boardLineIndex][boardColumnIndex];
        return currentTile;
    }

    /**
     * Adds a level of indirection in the index computation
     * In practice provides a rotation/ymmetry so that we need
     * to deal with one packing directionToPackInto only.
     * This operation also takes care of the conversion from (1..N) board
     * coordinates to the (0..N-1) Java array coordinates.
     *
     * NOTE: <b>NO CHECKS are made on parameter bounds.</b>
     *
     * @param lineIndex   must be in [1..sideSizeInSquares]
     * @param columnIndex must be in [1..sideSizeInSquares]
     * @return the columnIndex after rotation/symmetry
     */
    private int computeColumnIndex(int lineIndex, int columnIndex) {
        switch (directionToPackInto) {
            case RIGHT:
                return lineIndex - 1;
            case LEFT:
                return lineIndex - 1;
            case TOP:
                return columnIndex - 1;
            case BOTTOM:
                return sideSizeInSquares - columnIndex;     //Symmetry on a vertical axis
        }
        return 0; // NOT REACHED
    }


    /**
     * Adds a level of indirection in the index computation
     * In practice provides a rotation/symmetry so that we need
     * to deal with one packing directionToPackInto only.
     * This operation also takes care of the conversion from (1..N) board
     * coordinates to the (0..N-1) Java array coordinates.
     *
     * NOTE: <b>NO CHECKS are made on parameter bounds.</b>
     *
     * @param lineIndex   must be in [1..sideSizeInSquares]
     * @param columnIndex must be in [1..sideSizeInSquares]
     * @return the lineIndex after rotation/symmetry
     */
    private int computeLineIndex(int lineIndex, int columnIndex) {
        switch (directionToPackInto) {
            case LEFT:
                return columnIndex - 1;
            case RIGHT:
                return sideSizeInSquares - columnIndex;
            case BOTTOM:
                return lineIndex - 1;
            case TOP:
                return lineIndex - 1;
        }
        return 0; // NOT REACHED
    }

    private int computeLineIndex_BACKUP(int lineIndex, int columnIndex) {
        switch (directionToPackInto) {
            case LEFT:
                return lineIndex - 1;
            case RIGHT:
                return lineIndex - 1;
            case BOTTOM:
                return sideSizeInSquares - columnIndex;
            case TOP:
                return columnIndex - 1;
        }
        return 0; // NOT REACHED
    }

    /**
     * For testing purposes only.
     * Creates a board configuration using a matrix of ranks
     * <<! Le Rang 0 indique le manque de pièce !>>
     * @param rankMatrix a non null matrix reference, must match board size
     */
    public void loadBoard(int[][] rankMatrix) {
        for (int i = 0; i < sideSizeInSquares; i++) {
            for (int j = 0; j < sideSizeInSquares; j++) {
                if (rankMatrix[i][j] > 0) {
                    currentBoard[i][j] = new TileImpl(rankMatrix[i][j]);
                }
            }
        }
    }

    /**
     * For testing purposes only.
     * Writes the ranks of contents of the matrix into a logger
     *
     * @param logger  where to write into
     * @param message the message to write first before writing the contents of the board
     */
    public void printBoard(Logger logger, String message) {

        logger.info(message);
        for (int i = 0; i < sideSizeInSquares; i++) {
            StringBuffer outputBuffer = new StringBuffer();
            outputBuffer.append(i + 1);
            outputBuffer.append(":{");
            for (int j = 0; j < sideSizeInSquares; j++) {
                if (currentBoard[i][j] != null) {
                    outputBuffer.append(currentBoard[i][j].getRank());
                } else {
                    outputBuffer.append("0");
                }
            }
            outputBuffer.append("}");
            logger.info(outputBuffer.toString());
        }
    }



    //*___________________________________________________________________________________*//
    //*                                                                                   *//
    //* -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   - *//
    //* /!\ The code below has been written by Mónica Fernández - Mickaël Callimoutou /!\ *//
    //* -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   - *//
    //*                                                                                   *//
    //*___________________________________________________________________________________*//


    /**
     * @authors Mónica Fernández - Mickaël Callimoutou
     * Initialize the current board. [New Game]
     * Add two randoms Tiles to the current IBoard.
     * @return coordinates of the Two Tiles
     */
    @Override
    public void defaultInit() {
        /* Clean Boards */
        currentBoard = new ITile[sideSizeInSquares][sideSizeInSquares];
        nextBoard = new ITile[sideSizeInSquares][sideSizeInSquares];


        Random random = new Random();
        int line = random.nextInt(sideSizeInSquares);
        int column = random.nextInt(sideSizeInSquares);

        //* 0-> line ; 1-> column ; 2 -> Rank *//
        currentBoard[line][column] = new TileImpl(1);

        /* Make sure the second ITile isn't on the same position */
        boolean same_positions;
        int line2, column2;

        do{
            line2 = random.nextInt(sideSizeInSquares);
            column2 = random.nextInt(sideSizeInSquares);
            same_positions = (line2==line && column2==column);
        }while(same_positions);

        /* The second tile has one chance on two to be rank 2 */
        currentBoard[line2][column2] = new TileImpl(randomRank());

        //* Tells the controller that the initialization has correctly been done *//
        controller.updateView(currentBoard);
    }

    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * 80% of chance to get a rank 1,
     * 20% of chance to get a rank 2
     * @return a rank between 1 and 2
     */
    private int randomRank() {
        int[] proba_table = new int[5];
        proba_table[0] = 1;
        proba_table[1] = 1;
        proba_table[2] = 1;
        proba_table[3] = 2;
        proba_table[4] = 1;
        Random random = new Random();
        return proba_table[random.nextInt(5)];
    }


    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * (In order to refresh the view)
     * @return the board
     */
    @Override
    public ITile[][] getBoard() {
        return currentBoard;
    }


    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * @return True if the Game is Over
     */
    @Override
    public boolean gameIsOver() {
        for(int line=0; line < sideSizeInSquares; line++) {
            for(int column=0; column < sideSizeInSquares; column++) {
                if ( currentBoard[line][column] != null ) {
                    if (tileHasSimilarNeighbour(line, column, currentBoard[line][column].getRank())) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * Test if the ITile at tileLine and tileColumn of the IBoard has some similar (same rank) tiles around it
     * @param tileLine : Line of the tested ITile
     * @param tileColumn : Column of the tested ITile
     * @return
     */
    private boolean tileHasSimilarNeighbour(int tileLine, int tileColumn, int tileRank){
        int rankOfNeighbour=-1;

        if(tileLine != 0){ //If is not the first line we can compare with the neighbour on top.
            rankOfNeighbour = getRankOfTile(tileLine, tileColumn+1);
            if( rankOfNeighbour  == tileRank || rankOfNeighbour == -1){
                return true;
            }
        }

        if(tileLine != getSideSizeInSquares()-1) { //If is not the last line we can compare with the  neighbour on bottom.
            rankOfNeighbour = getRankOfTile(tileLine+2, tileColumn+1);
            if( rankOfNeighbour == tileRank || rankOfNeighbour == -1){
                return true;
            }
        }

        if(tileColumn != 0) {   //If is not the first column we can compare with the neighbour on left.
            rankOfNeighbour = getRankOfTile(tileLine+1, tileColumn);
            if( rankOfNeighbour == tileRank || rankOfNeighbour == -1){
                return true;
            }
        }

        if(tileColumn != getSideSizeInSquares()-1) { //If is not the last column we can compare with the neighbour on right.
            rankOfNeighbour = getRankOfTile(tileLine+1, tileColumn+2);
            if( rankOfNeighbour == tileRank || rankOfNeighbour == -1){
                return true;
            }
        }
        return false;
    }


    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * Add a new model.ITile to the board
     * (after a PackInto Event)
     * @return true if a new model.ITile has appeared - False if the board is full (GAME OVER)
     */
    private boolean popANewTileInBoard(ITile[][] board) {
        int rank = randomRank();
        ITile newTile = new TileImpl(rank);

        //* Look for a random empty case *//
        int[] freeCell = getFreeCell(board);
        if(freeCell[0] == -1 || freeCell[1] == -1) {
            return false;
        }

        writeTile(board, newTile, freeCell[0], freeCell[1]);
        return true;
    }


    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * look for a random empty cell in the board.
     * @param board
     * @return [line;column] of an empty cell in the board
     */
    private int[] getFreeCell(ITile[][] board) {
        //* getRandomOrder is used to explore randomly our IBoard  *//
        for(int line: getRandomOrder()){
            for(int column: getRandomOrder()) {
                if(readTile(board, line, column) == null){
                    return new int[]{line,column};
                }
            }
        }
        return new int[]{-1,-1};
    }

    /**
     * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
     * Get Random Order of exploration of our IBoard.
     * This method is used to explore randomly our board in order to generate/create a new Tile randomly
     * Exemple with SideSizeInSquares == 4 :    List( 2 ; 4 ; 1 ; 3 )
     *
     * @return an Unsorted List of Integer
     */
    private List<Integer> getRandomOrder(){
        List<Integer> randomList = new ArrayList<>();
        for (int i=1; i <= getSideSizeInSquares(); i++) {
            randomList.add(i);
        }
        //* Suffle the List *//
        Collections.shuffle(randomList);

        return randomList;
    }


    /**
     * Set the Controller
     * @param controller
     */
    @Override
    public void setController(IController controller) {
        this.controller = controller;
    }

    /**
     * Test if the cell has a tile.
     * @param line  line number to test
     * @param column column number to test
     * @return True if the cell has not a tile
     */
    @Override
    public boolean tileIsNull(int line, int column) {
        return getTile(line, column) == null;
    }


    /**
     *
     * @param line      position of the tile
     * @param column    position of the tile
     * @return Rank of the tile at position line and column
     */
    @Override
    public int getRankOfTile(int line, int column) {
        if ( ! tileIsNull(line, column)) {
            return getTile(line, column).getRank();
        }
        return -1;
    }


}
