package fr.istic.gli.monimick2048.viewcontrol.impl;

import fr.istic.gli.monimick2048.controller.IController;
import fr.istic.gli.monimick2048.viewcontrol.IViewController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

import fr.istic.gli.monimick2048.graphical.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


/**
 * @author Micka&euml;l CALLIMOUTOU &amp; M&oacute;nica FERN&Aacute;NDEZ
 * @version 1.0
 * @see fr.istic.gli.monimick2048.viewcontrol.IViewController
 */
public class ViewControllerImpl implements IViewController {

    /*  --  Attributes  --  */
    IController controller;

    /*  --  View  --  */
    @FXML
    GridPane viewBoard;
    @FXML
    Label gameover;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //* Label Expliciting the End of the Game *//
        gameover.setVisible(false);
        viewBoard.setVisible(true);
    }


    @Override
    public void setController(IController controller){
        this.controller = controller;
        initViewBoard();
    }

    /**
     * Initialize le model
     */
    private void initViewBoard(){
        controller.initModel();
    }

    /**
     * KeyPressed Event Listener
     *  (This method is linked to the ROOT NODE of our JavaFX application. see more in sample.FMXL)
     *  Only Reacts to Arrows' KeyEvent
     * @param event
     */
    @FXML
    public void keyPressed(KeyEvent event) {
        /**
        *  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
        * |   This application follow the MVP architecture                                     *
        * |   The View use the Enum Direction of the Controller, and not of the Board(Model)   *
        * |     Because We don't want the View to have ANY LINKS with the Board                *
        * |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
        */
        switch (event.getCode()) {
            case UP:
                controller.moveTilesTo(IController.Direction.UP);
                break;
            case DOWN:
                controller.moveTilesTo(IController.Direction.DOWN);
                break;
            case LEFT:
                controller.moveTilesTo(IController.Direction.LEFT);
                break;
            case RIGHT:
                controller.moveTilesTo(IController.Direction.RIGHT);
                break;
        }
    }


    /**
     * This method is what must be redefined if you want to plug another GUI JavaFX - FXML File to this application
     *      In this method, the Controller Refresh/Update the view after a packIntoDirection event
     *
     *  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
     * |         It is possible to implements some animations. (from the old state of the board, to the new State)  *
     * |                       We choose not to do it in order to keep it simple and efficient.                     *
     * |                        But here is a way to implement animations :                                         *
     * |    - Modify the Model :    [IBoard and BoardImpl] : GET the Movements information (Translation)            *
     * |                            Then the Controller could use those information and animate the View            *
     * |                                                                                                            *
     * |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ *
     *
     * This Method is called by the IController and Refresh this View
     * @param tiles : list of all non-empty Tiles in the current Board
     *              [0 -> line ; 1 -> column ; 2-> rank]
     */
    @Override
    public void updateView(ArrayList<int[]> tiles) {
        viewBoard.getChildren().clear();
        for(int tileIndex=0 ; tileIndex < tiles.size(); tileIndex++) {
            viewBoard.add(new TileGraphic(tiles.get(tileIndex)[2]), tiles.get(tileIndex)[0], tiles.get(tileIndex)[1]);
        }
    }


    /**
     * TODO: "NewGame Button"
     * If a NewGame button is added to the interface, this method could be used to clear the board
     * And regenerate a random start board with two random tiles.
     * @param event
     */
    @FXML
    public void newGameButtonAction(ActionEvent event) {
        viewBoard.getChildren().clear();
        controller.initModel();
    }



    /**
     * @authors Mónica Fernàndez - Mickael Callimoutou
     * TODO: "If you want CONTROLS in your GUI"
     * Action Event Listener
     *  ( if the GUI has some Buttons Controls, this method can be linked to them )
     * @param event
     */
    @FXML
    public void handleDirectionButtonAction(ActionEvent event) {
        String button = event.getSource().toString();

        /** Maybe it would be better and cleaner to define one Method per Event **/
        /** Because here we're adding constraints on the ID(or name) of the buttons : "downButton" ... **/

        if( button.contains("up")) {
            controller.moveTilesTo(IController.Direction.UP);

        }
        else if( button.contains("down")) {
            controller.moveTilesTo(IController.Direction.DOWN);
        }
        else if( button.contains("right")) {
            controller.moveTilesTo(IController.Direction.LEFT);
        }
        else { //** if (event.getSource() == "leftButton") **//
            controller.moveTilesTo(IController.Direction.RIGHT);
        }
    }



    /**
     * Ends the Game
     */
    @Override
    public void gameIsOver() {
        viewBoard.setDisable(false);
        // gameover.setVisible(true);

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Bye");
        alert.setHeaderText("Game is over");
        alert.setContentText("Thank's for playing :) ");
        alert.showAndWait();
        Platform.exit();
    }
}
